<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthToken extends Model
{
    /**
     * @var string
     */
    protected $table = 'auth_tokens';

    /**
     * @var array
     */
    protected $fillable = ['token', 'user_id'];

    /**
     * @param \App\User $user
     */
    public static function createFor(User $user)
    {
        static::where('user_id', $user->getAttribute('id'))->delete();

        return static::create(
            [
                'token' => str_random(35),
                'user_id' => $user->getAttribute('id'),
            ]
        );
    }

    /**
     * @param string $token
     * @return AuthToken|null
     */
    public static function findByToken($token)
    {
        if (!$token = static::where(compact('token'))->first()) {
            return null;
        }

        return $token;
    }
}
