<?php

namespace App\Forms;

use Mobileka\Lego\Form;
use Mobileka\Lego\Form\Components\Email;
use Mobileka\Lego\Form\Components\Submit;

class LoginForm extends Form
{
    /**
     * @var string
     */
    protected $method = 'POST';

    /**
     * @var string
     */
    protected $action = 'login';

    /**
     * LoginForm constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->addComponent((new Email('email'))->setLabel('Email Address'));
        $this->addComponent(new Submit);
    }
}
