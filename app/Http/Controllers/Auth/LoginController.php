<?php

namespace App\Http\Controllers\Auth;

use Alert;
use App\AuthToken;
use App\Forms\LoginForm;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ConfirmLoginRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Mail\Auth\LoginConfirmation;
use App\User;
use Auth;
use Mail;

class LoginController extends Controller
{
    /**
     * Login form
     *
     * @param \App\Forms\LoginForm $form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(LoginForm $form)
    {
        return view('auth.login', compact('form'));
    }

    /**
     * Issue a new login email
     *
     * @param \App\Http\Requests\Auth\LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function issue(LoginRequest $request)
    {
        $user = User::findByEmail($request->get('email'));
        $token = AuthToken::createFor($user);

        Mail::send(new LoginConfirmation($user, $token));

        return redirect()->route('login-issued');
    }

    /**
     * Tell the user that his email has been sent
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function issued()
    {
        return view('auth.login_issued');
    }

    /**
     * Confirm the authorization
     *
     * @param \App\Http\Requests\Auth\ConfirmLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function confirm(ConfirmLoginRequest $request)
    {
        $token = AuthToken::findByToken($request->get('token'));
        $user = User::findByEmail($request->get('email'));

        if ($user->getAttribute('id') !== $token->getAttribute('user_id')) {
            Alert::error('Sorry, we were unable to find your token');

            return redirect()->route('login');
        }

        Auth::login($user, true);
        $token->delete();

        Alert::success('Hello, ' . $user->getAttribute('name') . '!');

        return redirect()->route('login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::logout();
        Alert::success('You\'ve been successfully signed out');

        return redirect()->route('login');
    }

}
