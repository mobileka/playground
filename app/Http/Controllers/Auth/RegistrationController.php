<?php

namespace App\Http\Controllers\Auth;

use App\AuthToken;
use App\Forms\RegisterForm;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Mail\Auth\LoginConfirmation;
use App\User;
use Mail;

class RegistrationController extends Controller
{
    /**
     * @param \App\Http\Requests\Auth\RegistrationRequest $request
     */
    public function form(RegisterForm $form)
    {
        return view('auth.register', compact('form'));
    }

    /**
     * @param \App\Http\Requests\Auth\RegistrationRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function issue(RegistrationRequest $request)
    {
        $user = User::create(
            [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
            ]
        );

        $token = AuthToken::createFor($user);
        Mail::send(new LoginConfirmation($user, $token));

        return redirect()->route('login-issued');
    }
}
