<?php

namespace App\Http\Requests\Auth;

class ConfirmLoginRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255|exists:users,email',
            'token' => 'required|exists:auth_tokens,token'
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
