<?php

namespace App\Http\Requests\Auth;

class LoginRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255|exists:users,email',
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
