<?php

namespace App\Http\Requests\Auth;

class RegistrationRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
