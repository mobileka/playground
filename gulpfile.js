const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

elixir(mix => {
    mix.stylus('app.styl')
       .webpack('app.js');
});
