<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="description" content="Armen Markossyan"/>
    <meta name="author" content="Armen Markossyan"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    {{ Html::favicon('favicon/apple-icon-57x57.png', ['sizes'=> '57x57', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/apple-icon-60x60.png', ['sizes'=> '60x60', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/apple-icon-72x72.png', ['sizes'=> '72x72', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/apple-icon-76x76.png', ['sizes'=> '76x76', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/apple-icon-114x114.png', ['sizes'=> '114x114', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/apple-icon-120x120.png', ['sizes'=> '120x120', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/apple-icon-144x144.png', ['sizes'=> '144x144', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/apple-icon-152x152.png', ['sizes'=> '152x152', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/apple-icon-180x180.png', ['sizes'=> '180x180', 'rel' => 'apple-touch-icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/android-icon-192x192.png', ['sizes' => '192x192', 'rel' => 'icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/favicon-32x32.png', ['sizes' => '32x32', 'rel' => 'icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/favicon-96x96.png', ['sizes' => '96x96', 'rel' => 'icon', 'type' => 'image/png']) }}
    {{ Html::favicon('favicon/favicon-16x16.png', ['sizes' => '16x16', 'rel' => 'icon', 'type' => 'image/png']) }}
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    {{ Html::style('css/app.css') }}

    @yield('styles')
    @yield('scripts')

    <title>Armen Markossyan</title>
</head>
<body>
<header>
    <a href="/">{{ Html::image('images/logo.png', 'logo', ['class' => 'logo', 'width' => 75, 'height' => 65]) }}</a>
</header>
<div id="content">
    @yield('content')
</div>
<footer>
    <div class="copyright">&copy; 2016 <a href="http://armen.im/">Armen Markossyan</a></div>
</footer>
</body>
</html>
