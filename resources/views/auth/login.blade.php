@extends('auth.layout')

@section('content')
    <h1>Sign in</h1>

    <div class="form-container">
        {!! $form->build() !!}
    </div>
    <div class="register">New here? {{ link_to_route('registration', 'Create an account') }}.</div>
@endsection