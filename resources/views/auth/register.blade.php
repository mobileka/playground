@extends('auth.layout')

@section('content')
    <h1>Register</h1>

    <div class="form-container">
        {!! $form->build() !!}
    </div>
    <div class="register">Already have an account? {{ link_to_route('login', 'Sign in') }}.</div>
@endsection