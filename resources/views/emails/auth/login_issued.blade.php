Somebody has requested a sign in email for your account.<br>

Please follow this link to sign in:<br>

<a href="{{ route('login-confirmation').'?token='.$token->token.'&email='.$user->email }}">Sign In</a>