<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::group(
    ['namespace' => 'Auth', 'prefix' => 'auth'],
    function () {
        Route::group(
            ['middleware' => 'guest'],
            function () {
                Route::get('register', ['as' => 'registration', 'uses' => 'RegistrationController@form']);
                Route::post('register', 'RegistrationController@issue');

                Route::get('sign-in', ['as' => 'login', 'uses' => 'LoginController@form']);
                Route::post('sign-in', 'LoginController@issue');
                Route::get('sign-in/issued', ['as' => 'login-issued', 'uses' => 'LoginController@issued']);
                Route::get('confirm', ['as' => 'login-confirmation', 'uses' => 'LoginController@confirm']);
            }
        );

        Route::get('sign-out', ['as' => 'logout', 'uses' => 'LoginController@logout']);
    }
);
