<?php

namespace Mobileka\Lego;

use Mobileka\Lego\Form\Components\Component;

abstract class Base
{
    /**
     * @var array
     */
    protected $components = [];

    /**
     * @var string
     */
    protected $view;

    /**
     * @var string
     */
    protected $class;

    /**
     * @param \Mobileka\Lego\Form\Components\Component $component
     * @return $this
     */
    public function addComponent(Component $component)
    {
        $this->components[] = $component;

        return $this;
    }

    /**
     * @return array
     */
    public function getComponents(): array
    {
        return $this->components;
    }

    /**
     * @param array $components
     */
    public function setComponents(array $components)
    {
        $this->components = $components;
    }

    /**
     * @param string $class
     * @return \Mobileka\Lego\Base
     */
    public function addClass(string $class): Base
    {
        if (!$c = $this->getClass()) {
            $this->setClass($class);
        } else {
            $this->setClass($c . ' ' . $class);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass(string $class): Base
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @param string $view
     * @return \Mobileka\Lego\Base
     */
    public function setView(string $view): Base
    {
        $this->view = $view;

        return $this;
    }
}
