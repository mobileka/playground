<?php

namespace Mobileka\Lego;

class Form extends Base
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var string
     */
    protected $view = 'lego::form';

    /**
     * @var string
     */
    protected $class = 'form';

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * Form constructor.
     */
    public function __construct()
    {
        $this->setAction(route($this->action));
    }

    /**
     * @return \Illuminate\Support\HtmlString|string
     */
    public function build()
    {
        return view($this->getView(), ['form' => $this]);
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return \Mobileka\Lego\Form
     */
    public function setMethod(string $method): Form
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return \Mobileka\Lego\Form
     */
    public function setAction(string $action): Form
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     * @return \Mobileka\Lego\Form
     */
    public function setAttributes(array $attributes): Form
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * @param string $class
     * @return \Mobileka\Lego\Base
     */
    public function addClass(string $class): Base
    {
        if (!$existing = $this->getClass()) {
            $this->setClass($class);
        } else {
            $this->setClass($existing . ' ' . $class);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass(string $class): Base
    {
        $this->class = $class;

        return $this;
    }
}
