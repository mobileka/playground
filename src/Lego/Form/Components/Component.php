<?php

namespace Mobileka\Lego\Form\Components;

use App\Services\Str;

abstract class Component
{
    /**
     * @var string
     */
    protected $view;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $class = 'form-item';

    /**
     * @var string
     */
    protected $labelClass = 'form-label';

    /**
     * @var string
     */
    protected $errorClass = 'validation-error';

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        return view($this->getView(), ['component' => $this]);
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param string $view
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function setView($view): Component
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function setName($name): Component
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label ?: Str::ucfirst($this->getName());
    }

    /**
     * @param string $label
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function setLabel($label): Component
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title ?: $this->getName();
    }

    /**
     * @param string $title
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function setTitle($title): Component
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $class
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function addClass(string $class): Component
    {
        if (!$existing = $this->getClass()) {
            $this->setClass($class);
        } else {
            $this->setClass($existing . ' ' . $class);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function setClass(string $class): Component
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabelClass(): string
    {
        return $this->labelClass;
    }

    /**
     * @param string $labelClass
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function setLabelClass(string $labelClass): Component
    {
        $this->labelClass = $labelClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorClass(): string
    {
        return $this->errorClass;
    }

    /**
     * @param string $errorClass
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function setErrorClass(string $errorClass): Component
    {
        $this->errorClass = $errorClass;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     * @return \Mobileka\Lego\Form\Components\Component
     */
    public function setAttributes($attributes): Component
    {
        $this->attributes = $attributes;

        return $this;
    }
}
