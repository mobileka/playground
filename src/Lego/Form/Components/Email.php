<?php

namespace Mobileka\Lego\Form\Components;

class Email extends Component
{
    /**
     * @var string
     */
    protected $view = 'lego::form.email';

    /**
     * Email constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->setName($name);
    }
}
