<?php

namespace Mobileka\Lego\Form\Components;

class Submit extends Component
{
    /**
     * @var string
     */
    protected $view = 'lego::form.submit';

    /**
     * @var string
     */
    protected $class = 'form-button form-item';

    /**
     * @var string
     */
    protected $value = 'Submit';

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return \Mobileka\Lego\Form\Components\Submit
     */
    public function setValue(string $value): Submit
    {
        $this->value = $value;

        return $this;
    }


}
