<?php

namespace Mobileka\Lego\Form\Components;

class Text extends Component
{
    /**
     * @var string
     */
    protected $view = 'lego::form.text';

    /**
     * Email constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->setName($name);
    }
}
