<?php

namespace Mobileka\Lego\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class Lego extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::addNamespace('lego', app_path('../src/Lego/views'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
