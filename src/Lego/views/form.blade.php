{{ Form::open(['url' => $form->getAction(), 'method' => $form->getMethod(),'class' => $form->getClass()]) }}

@foreach($form->getComponents() as $component)
    {!! $component->render() !!}
@endforeach

{{ Form::close() }}