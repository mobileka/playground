<div class="form-group">
    <label class="{{ $component->getLabelClass() }}"
           for="{{ $component->getName() }}">{{ $component->getLabel() }}</label>
    <input class="{{ $component->getClass() }} @if ($errors->has($component->getName())) bad-form-item @endif" type="text"
           title="{{ $component->getTitle() }}"
           name="{{ $component->getName() }}"
           value="{{ old($component->getName(), '') }}">
    <div class="{{ $component->getErrorClass() }}">{{ $errors->first($component->getName()) }}</div>
</div>
